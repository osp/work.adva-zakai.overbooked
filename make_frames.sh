#! /bin/bash

if [ ! -d "projection/frames/1" ]; then
    echo "Made directory for frames wall 1"
    mkdir projection/frames/1
fi

if [ ! -d "projection/frames/2" ]; then
    echo "Made directory for frames wall 2"
    mkdir projection/frames/2
fi
if [ ! -d "projection/frames/3" ]; then
    echo "Made directory for frames wall 3"
    mkdir projection/frames/3
fi
if [ ! -d "projection/frames/4" ]; then
    echo "Made directory for frames wall 4"
    mkdir projection/frames/4
fi

echo "Dropping old frames"
rm -f projection/frames/1/*.png
rm -f projection/frames/2/*.png
rm -f projection/frames/3/*.png
rm -f projection/frames/4/*.png

echo "Generating frames wall 1"
# gs -sDEVICE=pngalpha -o projection/frames/1/frame-%03d.png -sDEVICE=pngalpha -r96 projection/pdf/1.pdf
echo "Generating frames wall 2"
# gs -sDEVICE=pngalpha -o projection/frames/2/frame-%03d.png -sDEVICE=pngalpha -r96 projection/pdf/2.pdf
echo "Generating frames wall 3"
# gs -sDEVICE=pngalpha -o projection/frames/3/frame-%03d.png -sDEVICE=pngalpha -r96 projection/pdf/3.pdf
echo "Generating frames wall 4"
gs -sDEVICE=pngalpha -o projection/frames/4/frame-%03d.png -sDEVICE=pngalpha -r96 projection/pdf/4.pdf


# mkdir projection/frames/4-rough
# rm projection/frames/4-rough/*.png
# gs -sDEVICE=pngalpha -o projection/frames/4-rough/frame-%03d.png -sDEVICE=pngalpha -r96 projection/pdf/4.rough.pdf
# avconv -r 2 -i projection/frames/4-rough/frame-%03d.png projection/videos/4.rough.mp4

if [ ! -d "projection/videos" ]; then
    echo "Made directory for videos"
    mkdir projection/videos
fi
# 
# rm projection/videos/1.mp4
# echo "Generating video wall 1"
# avconv -r 2 -i projection/frames/1/frame-%03d.png projection/videos/1.mp4

# rm projection/videos/2.mp4
# echo "Generating video wall 2"
# avconv -r 2 -i projection/frames/2/frame-%03d.png projection/videos/2.mp4

# rm projection/videos/3.mp4
# echo "Generating video wall 3"
# avconv -r 2 -i projection/frames/3/frame-%03d.png projection/videos/3.mp4
# 
rm projection/videos/4.mp4
echo "Generating video wall 4"
avconv -r 2 -i projection/frames/4/frame-%03d.png projection/videos/4.mp4