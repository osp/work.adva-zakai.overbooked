#! /bin/bash

if [ ! -d "pdf/combined" ]; then
    mkdir pdf/combined
fi

if [ ! -d "pdf/black" ]; then
    mkdir pdf/black
fi

if [ ! -d "pdf/gold" ]; then
    mkdir pdf/gold
fi

gs -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -sOutputFile=pdf/combined.clean.pdf pdf/combined.pdf
gs -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -sOutputFile=pdf/black.clean.pdf pdf/black.pdf
gs -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -sOutputFile=pdf/gold.clean.pdf pdf/gold.pdf

ospi plan_file=imposition_plan.py INPUT=pdf/combined.clean.pdf OUTPUT=pdf/combined/combined
ospi plan_file=imposition_plan.py INPUT=pdf/black.clean.pdf OUTPUT=pdf/black/black
ospi plan_file=imposition_plan.py INPUT=pdf/gold.clean.pdf OUTPUT=pdf/gold/gold
ospi plan_file=cover_imposition_plan.py

rm -f pdf/combined.zip pdf/black.zip pdf/gold.zip

zip -r pdf/combined.zip pdf/combined
zip -r pdf/black.zip pdf/black
zip -r pdf/gold.zip pdf/gold