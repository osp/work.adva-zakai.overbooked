from PIL import Image
import glob, os
from sys import argv
os.chdir(argv[1])

cnt = 0

for path in glob.glob("*.png"):
    cnt += 1

    if (cnt%100) == 0:
        print cnt

    im = Image.open(path)
    
    im.crop((0,0,1920,1080)).convert("L").save(path)