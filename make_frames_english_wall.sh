# #! /bin/bash
# # gs -sDEVICE=pdfwrite -dBATCH -dNOPAUSE -sOutputFile=projection-english/pdf/1.pdf projection-english/pdf/1.dirty.pdf
# # gs -sDEVICE=pdfwrite -dBATCH -dNOPAUSE -sOutputFile=projection-english/pdf/2.pdf projection-english/pdf/2.dirty.pdf
# # gs -sDEVICE=pdfwrite -dBATCH -dNOPAUSE -sOutputFile=projection-english/pdf/3.pdf projection-english/pdf/3.dirty.pdf
# # gs -sDEVICE=pdfwrite -dBATCH -dNOPAUSE -sOutputFile=projection-english/pdf/4.pdf projection-english/pdf/4.dirty.pdf
wall=$1

if [ ! -d "projection-english/frames" ]; then
    mkdir projection-english/frames
fi

if [ ! -d "projection-english/frames/${wall}" ]; then
    echo "Made directory for frames wall ${wall}"
    mkdir projection-english/frames/$wall
fi



echo "Dropping old frames"
rm -f projection-english/frames/$wall/*.png
echo "Generating frames wall ${wall}"
gs -sDEVICE=pngalpha -o projection-english/frames/$wall/frame-%04d.png -sDEVICE=pngalpha -r96 projection-english/pdf/$wall.pdf

echo "Fixing frames wall ${wall}"
python forceFixImages.py projection-english/frames/$wall

# mkdir projection-english/frames/4-rough
# rm projection-english/frames/4-rough/*.png
# gs -sDEVICE=pngalpha -o projection-english/frames/4-rough/frame-%03d.png -sDEVICE=pngalpha -r96 projection-english/pdf/4.rough.pdf
# avconv -r 2 -i projection-english/frames/4-rough/frame-%03d.png projection/videos/4.rough.mp4

if [ ! -d "projection-english/videos" ]; then
    echo "Made directory for videos"
    mkdir projection-english/videos
fi


rm projection-english/videos/$wall.mp4
echo "Generating video wall ${wall}"
avconv  -r 2 -i projection-english/frames/$wall/frame-%04d.png -pix_fmt yuv444p -r 2 projection-english/videos/$wall.mp4