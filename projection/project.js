function (window, document) {
    'use strict';
    
    function getScale(transform) {
        var test = /matrix\(((?:\d+\.)?\d+),\s*((?:\d+\.)?\d+),\s*((?:\d+\.)?\d+),\s*((?:\d+\.)?\d+),\s*((?:\d+\.)?\d+),\s*((?:\d+\.)?\d+)\)/
        
        if (test.test(transform)) {
            var matches = test.exec(transform);
            return [ parseFloat(matches[1]), parseFloat(matches[4]) ];
        }
        
        return [ 1, 1 ];
    }

    function position (x, y) {
        var sections = document.getElementsByTagName('section'),
            left = x, top = y,
            offsetLeft, offsetTop;
        
        for (var i=0; i < sections.length; i++) {
            var anchor = sections[i].getElementsByClassName('anchor')[0],
                rect = anchor.getBoundingClientRect(),
                paragraph = sections[i].getElementsByTagName('p')[0];
            
            offsetLeft = left - anchor.offsetLeft;
            offsetTop = top - anchor.offsetTop;
                
            
            var style = window.getComputedStyle(paragraph),
                scale = getScale(style.transform),
                centerDistanceX = (anchor.offsetLeft + ((rect.width * .5) / scale[0])) - parseInt(paragraph.clientWidth) / 2,
                scaleCorrectionX = centerDistanceX * (parseFloat(scale[0]) - 1),
                offsetLeft = offsetLeft - scaleCorrectionX,
                centerDistanceY = (anchor.offsetTop + ((rect.height * .5) / scale[1])) - parseInt(paragraph.clientHeight) / 2,
                scaleCorrectionY = centerDistanceY * (parseFloat(scale[1]) - 1),
                offsetTop = offsetTop - scaleCorrectionY;
                
            paragraph.style.setProperty('left', offsetLeft + 'px');
            paragraph.style.setProperty('top', offsetTop + 'px');
        }
    }

    function timeFrames () {
        console.log('Called');
        var section, duration, frames, clones = [],
            sections = document.getElementsByTagName('section'),
            frameLength = 15;
        
        for (var i=0; i < sections.length; i++) {
            section = sections[i],
            duration = section.getAttribute('duration'),
            frames = Math.ceil(duration / frameLength);
            
            for (var f=0; f < frames; f++) {
                clones.push({
                    clone: section.cloneNode(true),
                    reference: section,
                    parent: section.parentElement
                });
            }
        }
        
        for (var c=0; c < clones.length; c++) {
            var row = clones[c];
            row.parent.insertBefore(row.clone, row.reference);
        }
    }
    
    window.timeFrames = 
}(window, document);