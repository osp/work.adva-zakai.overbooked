(function($, window) {
    'use strict';
    
    // ________________________________ INIT __________________________________ //
    // Creating crop marks
    $('#master-page').append('<div class="crops"><div class="crop-top-left"><span class="bleed"></span></div><div class="crop-top-right"><span class="bleed"></span></div><div class="crop-bottom-right"><span class="bleed"></span></div><div class="crop-bottom-left"><span class="bleed"></span></div></div>')
    $('#master-page').append('<div class="center-crop left"><div class="vertical"></div><div class="horizontal"></div><div class="circle"></div></div>');
    $('#master-page').append('<div class="center-crop top"><div class="vertical"></div><div class="horizontal"></div><div class="circle"></div></div>');
    $('#master-page').append('<div class="center-crop right"><div class="vertical"></div><div class="horizontal"></div><div class="circle"></div></div>');
    $('#master-page').append('<div class="center-crop bottom"><div class="vertical"></div><div class="horizontal"></div><div class="circle"></div></div>');
    
    // Detach master so it won't confuse the content flow
    var $page, $master = $('#master-page').detach();
    
    var insertCredits = function ($page) {
        $page.find(".body").append('<div class="credits">créé en automne 2015 dans le cadre de l\'exposition et de la performance last seen standing between brackets, de et avec :<br />adva zakai, futurarenner regular, <span class="reference">1</span> the spell of the sensous / david abram, pierre huyghebaert &amp; gijs de heij / osp (be),<br /><span class="reference">2</span> metaphors we live by / george lakoff &amp; mark johnson, frans masereel center – print art center &amp; residency (be),<br />raphaële jeune, news from the sun / j.g ballard, phakt – centre culturel colombier (fr), david weber-krebs, html2print,<br /><span class="reference">3</span> the paris review (no. 197) / william gibson, olga bientz, <span class="reference">4</span> free art licence, shila anaraki, la possibilité d\'une île / michel&nbsp;houellebecq, fedrigoni freelife vellum white paper, risographie, cover: sérigraphie .../100, isbn 9789082353839</p>');
    }
    
    // Cloning the master page
    for (var i = 1; i <= nb_page; i++){
        $page = $master.clone()
                    .attr('id','page-' + i)
                    .attr('data-page', i);
        
        if (i == nb_page) {
            insertCredits($page);
        }

        $('#pages').append($page);
        
    }
    
    // loads stories
    $('#stories [data-src]').each(function() {
        var src = $(this).attr('data-src');
        $(this).load(src, function() {});
    })   
})($, window);
