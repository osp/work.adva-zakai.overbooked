# python test imposition plan
# Identity imposition plan

sinfo = pdf_info.extract('pdf/projection/1.pdf')
bbox = sinfo["page_size"][0][1]
pcount = sinfo["page_count"]
twidth = bbox['width'] * 4
theight = bbox['height']

scale = [1,1]

cropbox = {'left': 0, 'bottom': 0, 'width': bbox['width'], 'height': bbox['height']}

imposition_plan = []

def add_page (sdoc, source_page, offset, scale, cropbox):
    return {
        "source_document" : sdoc,
        "source_page" : source_page,
        "crop_box" : cropbox,
        "translate" : offset,
        "rotate" : 0,
        "scale" : scale
    }


for page in range(0, pcount):
    imposition_plan.append({
        "target_document" : 'pdf/projection/combined.pdf',
        "target_page_width" : twidth,
        "target_page_height" : theight,
        "pages" : [
            add_page('pdf/projection/1.pdf', page, [0,0], scale, cropbox),
            add_page('pdf/projection/2.pdf', page, [bbox['width'],0], scale, cropbox),
            add_page('pdf/projection/3.pdf', page, [bbox['width'] * 2,0], scale, cropbox),
            add_page('pdf/projection/4.pdf', page, [bbox['width'] * 3,0], scale, cropbox),
        ]
    })