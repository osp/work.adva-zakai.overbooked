# python test imposition plan
# Identity imposition plan

sinfo = pdf_info.extract('pdf/projection-en/1.clean.pdf')
bbox = sinfo["page_size"][0][1]
pcount = sinfo["page_count"]
twidth = bbox['width'] * 2
theight = bbox['height'] * 2

scale = [1,1]

cropbox = {'left': 0, 'bottom': 0, 'width': bbox['width'], 'height': bbox['height']}

imposition_plan = []

def add_page (sdoc, source_page, offset, scale, cropbox):
    return {
        "source_document" : sdoc,
        "source_page" : source_page,
        "crop_box" : cropbox,
        "translate" : offset,
        "rotate" : 0,
        "scale" : scale
    }


for page in range(0, pcount):
    imposition_plan.append({
        "target_document" : 'pdf/projection-en/combined.pdf',
        "target_page_width" : twidth,
        "target_page_height" : theight,
        "pages" : [
            add_page('pdf/projection-en/1.clean.pdf', page, [0, 0], scale, cropbox),
            add_page('pdf/projection-en/2.clean.pdf', page, [bbox['width'], 0], scale, cropbox),
            add_page('pdf/projection-en/3.clean.pdf', page, [0, bbox['height']], scale, cropbox),
            add_page('pdf/projection-en/4.clean.pdf', page, [bbox['width'], bbox['height']], scale, cropbox)
        ]
    })
