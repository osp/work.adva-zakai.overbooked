#! /bin/bash

echo 'Normal PDF...'
mutool run combine.js

echo 'Running nup...'
pdfjam pdf-short/combined.pdf --nup 2x2 --papersize '{1600px,1200px}' --outfile pdf-short/combined-nup.pdf
