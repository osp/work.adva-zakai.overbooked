// A re-implementation of "mutool merge" in JavaScript.

function graftObject(dstDoc, srcDoc, srcObj, map) {
	var srcNum, dstRef, dstObj
	if (!map)
		map = []
	if (srcObj.isIndirect()) {
		srcNum = srcObj.asIndirect()
		if (map[srcNum])
			return map[srcNum]
		map[srcNum] = dstRef = dstDoc.createObject()
		dstRef.writeObject(graftObject(dstDoc, srcDoc, srcObj.resolve(), map))
		if (srcObj.isStream())
			dstRef.writeRawStream(srcObj.readRawStream())
		return dstRef
	}
	if (srcObj.isArray()) {
		dstObj = dstDoc.newArray()
		srcObj.forEach(function (key, val) {
			dstObj[key] = graftObject(dstDoc, srcDoc, val, map)
		})
		return dstObj
	}
	if (srcObj.isDictionary()) {
		dstObj = dstDoc.newDictionary()
		srcObj.forEach(function (key, val) {
			dstObj[key] = graftObject(dstDoc, srcDoc, val, map)
		})
		return dstObj
	}
	return srcObj /* primitive objects are not bound to a document */
}


function copyPage(dstDoc, srcDoc, pageNumber, map) {
	var srcPage, dstPage, offset	
	srcPage = srcDoc.findPage(pageNumber)
	dstPage = dstDoc.newDictionary()
	dstPage.Type = dstDoc.newName("Page")
	
	if (srcPage.MediaBox) dstPage.MediaBox = graftObject(dstDoc, srcDoc, srcPage.MediaBox, map)
	if (srcPage.Rotate) dstPage.Rotate = graftObject(dstDoc, srcDoc, srcPage.Rotate, map)
	if (srcPage.Resources) dstPage.Resources = graftObject(dstDoc, srcDoc, srcPage.Resources, map)
	if (srcPage.Contents) dstPage.Contents = graftObject(dstDoc, srcDoc, srcPage.Contents, map)

	dstDoc.insertPage(-1, dstDoc.addObject(dstPage))
}

function impose() {
	var combined, one, two, three, four

	combined = new PDFDocument()

  	one = new PDFDocument('pdf-short/1.pdf')
	two = new PDFDocument('pdf-short/2.pdf')
	three = new PDFDocument('pdf-short/3.pdf')
	four = new PDFDocument('pdf-short/4.pdf')

	var k, n = one.countPages(), srcmap = []

	for (k = 0; k<n; ++k) {
		print(k);
		if (k < 5) {
			copyPage(combined, one, k, [])
			copyPage(combined, two, k, [])
			copyPage(combined, three, k, [])
			copyPage(combined, four, k, [])
		} else if (k == 5) {
			copyPage(combined, one, k, [])
			copyPage(combined, two, k, [])
			copyPage(combined, three, k, [])
			copyPage(combined, four, k, [])
			
			copyPage(combined, one, k, [])
			copyPage(combined, two, k, [])
			copyPage(combined, three, k, [])
			copyPage(combined, four, k+1, [])
		} else {
			copyPage(combined, one, k, [])
			copyPage(combined, two, k, [])
			copyPage(combined, three, k, [])
			copyPage(combined, four, k+1, [])
		}
	}

	combined.save('pdf-short/combined.pdf', 'compress')
	
}

impose()
