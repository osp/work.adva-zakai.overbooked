# #! /bin/bash
# # gs -sDEVICE=pdfwrite -dBATCH -dNOPAUSE -sOutputFile=projection-english/pdf/1.pdf projection-english/pdf/1.dirty.pdf
# # gs -sDEVICE=pdfwrite -dBATCH -dNOPAUSE -sOutputFile=projection-english/pdf/2.pdf projection-english/pdf/2.dirty.pdf
# # gs -sDEVICE=pdfwrite -dBATCH -dNOPAUSE -sOutputFile=projection-english/pdf/3.pdf projection-english/pdf/3.dirty.pdf
# # gs -sDEVICE=pdfwrite -dBATCH -dNOPAUSE -sOutputFile=projection-english/pdf/4.pdf projection-english/pdf/4.dirty.pdf

if [ ! -d "projection-english/frames" ]; then
    mkdir projection-english/frames
fi

if [ ! -d "projection-english/frames/1" ]; then
    echo "Made directory for frames wall 1"
    mkdir projection-english/frames/1
fi

if [ ! -d "projection-english/frames/2" ]; then
    echo "Made directory for frames wall 2"
    mkdir projection-english/frames/2
fi

if [ ! -d "projection-english/frames/3" ]; then
    echo "Made directory for frames wall 3"
    mkdir projection-english/frames/3
fi
if [ ! -d "projection-english/frames/4" ]; then
    echo "Made directory for frames wall 4"
    mkdir projection-english/frames/4
fi

echo "Dropping old frames"
rm -f projection-english/frames/1/*.png
rm -f projection-english/frames/2/*.png
rm -f projection-english/frames/3/*.png
rm -f projection-english/frames/4/*.png

echo "Generating frames wall 1"
gs -sDEVICE=pngalpha -o projection-english/frames/1/frame-%04d.png -sDEVICE=pngalpha -r96 projection-english/pdf/1.pdf
echo "Generating frames wall 2"
gs -sDEVICE=pngalpha -o projection-english/frames/2/frame-%04d.png -sDEVICE=pngalpha -r96 projection-english/pdf/2.pdf
echo "Generating frames wall 3"
gs -sDEVICE=pngalpha -o projection-english/frames/3/frame-%04d.png -sDEVICE=pngalpha -r96 projection-english/pdf/3.pdf
echo "Generating frames wall 4"
gs -sDEVICE=pngalpha -o projection-english/frames/4/frame-%04d.png -sDEVICE=pngalpha -r96 projection-english/pdf/4.pdf

echo "Fixing frames wall 1"
python forceFixImages.py projection-english/frames/1
echo "Fixing frames wall 2"
python forceFixImages.py projection-english/frames/2
echo "Fixing frames wall 3"
python forceFixImages.py projection-english/frames/3
echo "Fixing frames wall 4"
python forceFixImages.py projection-english/frames/4

# mkdir projection-english/frames/4-rough
# rm projection-english/frames/4-rough/*.png
# gs -sDEVICE=pngalpha -o projection-english/frames/4-rough/frame-%03d.png -sDEVICE=pngalpha -r96 projection-english/pdf/4.rough.pdf
# avconv -r 2 -i projection-english/frames/4-rough/frame-%03d.png projection/videos/4.rough.mp4

if [ ! -d "projection-english/videos" ]; then
    echo "Made directory for videos"
    mkdir projection-english/videos
fi


rm projection-english/videos/1.mp4
echo "Generating video wall 1"
avconv  -r 2 -i projection-english/frames/1/frame-%04d.png -pix_fmt yuv444p -r 2 projection-english/videos/1.mp4

rm projection-english/videos/2.mp4
echo "Generating video wall 2"
avconv -r 2 -i projection-english/frames/2/frame-%04d.png -pix_fmt yuv444p -r 2 projection-english/videos/2.mp4

rm projection-english/videos/3.mp4
echo "Generating video wall 3"
avconv  -r 2 -i projection-english/frames/3/frame-%04d.png -pix_fmt yuv444p -r 2 projection-english/videos/3.mp4

rm projection-english/videos/4.mp4
echo "Generating video wall 4"
avconv  -r 2 -i projection-english/frames/4/frame-%04d.png -pix_fmt yuv444p -r 2 projection-english/videos/4.mp4