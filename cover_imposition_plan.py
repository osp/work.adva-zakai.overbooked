# python test imposition plan
# Identity imposition plan

sinfo = pdf_info.extract('pdf/gold/gold-cover-recto.pdf')
bbox = sinfo["page_size"][0][1]
pcount = sinfo["page_count"]
twidth = 420 * 2.83465
theight = (297 * 2) * 2.83465

scale = [1,1]

cropbox = {'left': 0, 'bottom': 0, 'width': bbox['width'], 'height': bbox['height']}

verticaloffset = bbox['height']
offsetleft = 0
offsetright = [(twidth / scale[0]) / 2, verticaloffset]

imposition_plan = []

def add_page (sdoc, source_page, offset, scale, cropbox):
    return {
        "source_document" : sdoc,
        "source_page" : source_page,
        "crop_box" : cropbox,
        "translate" : offset,
        "rotate" : 0,
        "scale" : scale
    }


imposition_plan.append({
    "target_document" : 'pdf/gold/A2-gold-recto.pdf',
    "target_page_width" : twidth,
    "target_page_height" : theight,
    "pages" : [
        add_page('pdf/gold/gold-cover-recto.pdf', 0, [0,0], scale, cropbox),
        add_page('pdf/gold/gold-cover-recto.pdf', 0, [0,verticaloffset], scale, cropbox),
    ]
})
    
imposition_plan.append({
    "target_document" : 'pdf/gold/A2-gold-verso.pdf',
    "target_page_width" : twidth,
    "target_page_height" : theight,
    "pages" : [
        add_page('pdf/gold/gold-cover-verso.pdf', 0, [0,0], scale, cropbox),
        add_page('pdf/gold/gold-cover-verso.pdf', 0, [0,verticaloffset], scale, cropbox),
    ]
})
    
imposition_plan.append({
    "target_document" : 'pdf/black/A2-black-recto.pdf',
    "target_page_width" : twidth,
    "target_page_height" : theight,
    "pages" : [
        add_page('pdf/black/black-cover-recto.pdf', 0, [0,0], scale, cropbox),
        add_page('pdf/black/black-cover-recto.pdf', 0, [0,verticaloffset], scale, cropbox),
    ]
})
    
imposition_plan.append({
    "target_document" : 'pdf/black/A2-black-verso.pdf',
    "target_page_width" : twidth,
    "target_page_height" : theight,
    "pages" : [
        add_page('pdf/black/black-cover-verso.pdf', 0, [0,0], scale, cropbox),
        add_page('pdf/black/black-cover-verso.pdf', 0, [0,verticaloffset], scale, cropbox),
    ]
})
