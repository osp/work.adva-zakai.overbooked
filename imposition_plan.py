# python test imposition plan
# Identity imposition plan


sdoc = params["INPUT"]
tdoc = params["OUTPUT"]
sinfo = pdf_info.extract(sdoc)
bbox = sinfo["page_size"][0][1]
pcount = sinfo["page_count"]
twidth = 420 * 2.83465
theight = 297 * 2.83465

scale = [(210 * 2.83465) / bbox['width'], (210 * 2.83465) / bbox['width']]

nwidth = bbox['width'] * scale[0]
nheight = bbox['height'] * scale[1]

cropbox = {'left': 0, 'bottom': 0, 'width': bbox['width'], 'height': bbox['height']}

verticaloffset = (theight - nheight) / 2
offsetleft = [0, verticaloffset]
offsetright = [(twidth / scale[0]) / 2, verticaloffset]

imposition_plan = []

def add_page (sdoc, source_page, offset, scale, cropbox):
    return {
        "source_document" : sdoc,
        "source_page" : source_page,
        "crop_box" : cropbox,
        "translate" : offset,
        "rotate" : 0,
        "scale" : scale
    }

cnt = 1

for i in range(2,45,4):
    imposition_plan.append({
        "target_document" : '{0}-sheet-{1:0>3}-{2},{3}.pdf'.format(tdoc, cnt, i+4, i+1),
        "target_page_width" : twidth,
        "target_page_height" : theight,
        "pages" : [
            add_page(sdoc, i+3, offsetleft, scale, cropbox),
            add_page(sdoc, i, offsetright, scale, cropbox)
        ]
    })
    
    cnt += 1
    
    imposition_plan.append({
        "target_document" : '{0}-sheet-{1:0>3}-{2},{3}.pdf'.format(tdoc, cnt, i+2, i+3),
        "target_page_width" : twidth,
        "target_page_height" : theight,
        "pages" : [
            add_page(sdoc, i+1, offsetleft, scale, cropbox),
            add_page(sdoc, i+2, offsetright, scale, cropbox)
        ]
    })
    
    cnt += 1


imposition_plan.append({
    "target_document" : '{0}-sheet-{1:0>3}-47,.pdf'.format(tdoc, cnt),
    "target_page_width" : twidth,
    "target_page_height" : theight,
    "pages" : [
        add_page(sdoc, 46, offsetleft, scale, cropbox)
    ]
})
    
cnt += 1
    
imposition_plan.append({
    "target_document" : '{0}-sheet-{1:0>3}-,48.pdf'.format(tdoc, cnt),
    "target_page_width" : twidth,
    "target_page_height" : theight,
    "crop_box" : {"left":0,"bottom":0,"width":(210*2.83465), "height":(297*2.83465)},
    "pages" : [
        add_page(sdoc, 47, offsetright, scale, cropbox)
    ]
})

imposition_plan.append({
    "target_document" : '{0}-cover-recto.pdf'.format(tdoc),
    "target_page_width" : twidth,
    "target_page_height" : theight,
    "pages" : [
        add_page(sdoc, 49, offsetleft, scale, cropbox),
        add_page(sdoc, 0, offsetright, scale, cropbox)
    ]
})
    
imposition_plan.append({
    "target_document" : '{0}-cover-verso.pdf'.format(tdoc),
    "target_page_width" : twidth,
    "target_page_height" : theight,
    "crop_box" : {"left":0,"bottom":0,"width":(210*2.83465), "height":(297*2.83465)},
    "pages" : [
        add_page(sdoc, 1, offsetleft, scale, cropbox),
        add_page(sdoc, 48, offsetright, scale, cropbox)
    ]
})
